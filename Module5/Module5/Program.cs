﻿using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace Module5
{
    class Program
    {
        public class Cell
        {            
            public bool bomb = false;
        }
        public class Player
        {
            public (int, int) coordinates;
            public int healthPoints;
        }
        public class Bomb
        {            
            public int damage;
            public bool isItFound = false;
        }
        static void Main(string[] args)
        {
            Console.SetWindowSize(52, 34);
            Console.Title = "Save the princess";
            string call = "The princess is in the right bottom corner.\nGet there and save her!";
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.White;                
                Console.WriteLine("Welcome to the game!\nType 'Start' to start the game or 'Exit'\nto exit the game");
                string input = Console.ReadLine();
                bool start = false;
                switch (input)
                {
                    case "start":
                    case "START":
                    case "Start": start = true; Console.WriteLine(call + "\nPress any key to start"); Console.ReadKey(); break;
                    case "Exit":
                    case "exit":
                    case "EXIT": break;  
                }
                if (start)
                {
                    StartGame();
                }
                else break;
            }
                
        }
        public static void StartGame()
        {
            Cell[,] field = new Cell[10, 10];
            
            Bomb[,] bombs = new Bomb[10,10];
            Player player = new Player();
            player.healthPoints = 10;
            player.coordinates = (0, 0);
            InitializeArrayOfCells(field);
            InitializeArrayOfBombs(bombs);
            PlaceBombs(ref field, ref bombs);
            
            while (true)
            {
                CreateFieldInConsole(player, bombs);
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.UpArrow: MoveUp(ref player); break;
                    case ConsoleKey.DownArrow: MoveDown(ref player); break;
                    case ConsoleKey.LeftArrow: MoveLeft(ref player); break;
                    case ConsoleKey.RightArrow: MoveRight(ref player); break;
                }
                if (field[player.coordinates.Item1, player.coordinates.Item2].bomb)
                {
                    player.healthPoints -= bombs[player.coordinates.Item1, player.coordinates.Item2].damage;
                    bombs[player.coordinates.Item1, player.coordinates.Item2].isItFound = true;
                }
                if(player.healthPoints <= 0)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red; 
                    Console.WriteLine("GAME OVER");
                    break;
                }
                if (player.coordinates == (9,9))
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("YOU WON!");
                    break;
                }
            }

        }
        public static void PlaceBombs(ref Cell[,] field, ref Bomb[,] bombs)
        {
            Random random = new Random();
            int numberOfBomb = 0;
            int x, y, damageOfBomb;
            for (; numberOfBomb < 10; )
            {
                x = random.Next(0, 9);
                y = random.Next(0, 9);                   
                if (!field[x,y].bomb && (x, y) != (0, 0) && (x,y) != (9,9))
                {
                    field[x, y] = new Cell();
                    damageOfBomb = random.Next(1, 10);
                    field[x, y].bomb = true;                    
                    bombs[x,y].damage = damageOfBomb;
                    numberOfBomb++;
                }
            }
        }
        public static void MoveUp(ref Player player)
        {
            if (player.coordinates.Item1 > 0)
                player.coordinates.Item1--;
        }
        public static void MoveDown(ref Player player)
        {
            if (player.coordinates.Item1 < 10 )
                player.coordinates.Item1++;
        }
        public static void MoveRight(ref Player player)
        {
            if (player.coordinates.Item2 < 10)
                player.coordinates.Item2++;
        }
        public static void MoveLeft(ref Player player)
        {
            if (player.coordinates.Item2 > 0)
                player.coordinates.Item2--;
        }
        public static void CreateFieldInConsole(Player player, Bomb[,] bombs)
        {

            Console.SetWindowSize(52, 34);
            Console.Clear();
            string field ="";
            for (int i = 0; i < 10; i++)
            { 
                for (int j = 0; j <10; j++)
                {
                    field += "┌───┐";
                }
                field += "\n";
                for (int j = 0; j < 10; j++)
                {
                    if (player.coordinates == (i, j))
                        field += "| ■ |";
                    else
                    {
                        if (bombs[i, j].isItFound)
                        { 
                            field += "| O |"; 
                        }
                        else
                            field += "|   |";
                    }
                }
                field += "\n";
                for (int j = 0; j < 10; j++)
                {
                    field += "└───┘";
                }
                field += "\n";
                
            }
            field += "Player health = " + player.healthPoints.ToString() + "\n";
            Console.WriteLine(field);
        }
        public static void InitializeArrayOfCells(Cell[,] field)
        {
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    field[i, j] = new Cell();
                }
            }
        }
        public static void InitializeArrayOfBombs(Bomb[,] bombs)
        {
            for (int i = 0; i < bombs.GetLength(0); i++)
            {
                for (int j = 0; j < bombs.GetLength(1); j++)
                {
                    bombs[i,j] = new Bomb();
                }
            }
        }
    }
}
